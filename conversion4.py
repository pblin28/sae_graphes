import json
import networkx as nx
import matplotlib.pyplot as plt

#1
def fichier_json_vers_networkx(json_data):

    graph = nx.Graph()

    # Charger les données JSON
    json_list = json_data.strip().split('\n')
    actors_dico = {}

    for ligne in json_list:
        data = json.loads(ligne)

        acteurs = data['cast']

        if len(acteurs) > 1:
            for acteur in acteurs:
                if acteur not in actors_dico:
                    actors_dico[acteur] = 1
                else:
                    actors_dico[acteur] += 1

        for actor in acteurs:
            graph.add_node(actor)

            for actor1 in acteurs:
                if actor != actor1:
                    graph.add_edge(actor, actor1)

            for actor2 in acteurs:
                if actor != actor2:
                    graph.add_edge(actor, actor2)
            
    return graph, actors_dico

def afficher_networkx(graph):
    nx.draw(graph, with_labels=True)
    plt.show()


#2
def collab_en_communs(graph, acteur1, acteur2):
    if acteur1 not in graph.nodes or acteur2 not in graph.nodes:
        return []

    acteur1_v = set(graph.neighbors(acteur1))
    acteur2_v = set(graph.neighbors(acteur2))

    collab_commun = acteur1_v.intersection(acteur2_v)
    
    return list(collab_commun)
    


def collaborateurs_proches(graph, acteur, k):
    if acteur not in graph.nodes:
         return []

    voisins = set(graph.neighbors(acteur))
    voisins_k = set()

    for i in range(k):
        for voisin in voisins:
            voisins_k.add(voisin)

        voisins = voisins_k.copy()
        voisins_k.clear()

    return list(voisins)

def max_distance_entre_deux_acteurs(graph):
    max_distance = 0

    for acteur1 in graph.nodes:
        for acteur2 in graph.nodes:
            distance = trouve_distance_entre_2_acteurs(graph, acteur1, acteur2)
            if distance > max_distance:
                max_distance = distance

    return max_distance


def trouve_distance_entre_2_acteurs(graph, acteur1, acteur2):
    if acteur1 not in graph.nodes or acteur2 not in graph.nodes:
        return []

    if acteur1 == acteur2:
        return 0

    distance = 0
    voisins = set(graph.neighbors(acteur1))
    voisins_k = set()

    while acteur2 not in voisins:
        for voisin in voisins:
            voisins_k.add(voisin)

        voisins = voisins_k.copy()
        voisins_k.clear()
        distance += 1

    return distance

#4   
def acteur_le_plus_central_du_graphe(graph):
    centralities = nx.closeness_centrality(graph)
    most_central_actor = max(centralities, key=centralities.get)
    return most_central_actor


def centrality_of_an_actor(graph):
    # Vérifier si le graphe est connecté
    if nx.is_connected(graph):
        # Calculer la centralité de proximité
        centralities = nx.closeness_centrality(graph)

        # Trouver l'acteur le plus central
        most_central_actor = max(centralities, key=centralities.get)

        # Trouver la plus grande distance pour l'acteur le plus central
        max_distance = nx.eccentricity(graph, v=most_central_actor)

        print("Acteur le plus central :", most_central_actor)
        print("Plus grande distance :", max_distance)
    else:
        # Calculer la centralité de proximité et la plus grande distance pour chaque composant connecté
        components = nx.connected_components(graph)
        for component in components:
            subgraph = graph.subgraph(component)
            centralities = nx.closeness_centrality(subgraph)
            most_central_actor = max(centralities, key=centralities.get)
            max_distance = nx.eccentricity(subgraph, v=most_central_actor)
            print("Acteur le plus central dans le composant :", most_central_actor)
            print("Plus grande distance dans le composant :", max_distance)

# Assurez-vous d'avoir déjà créé le graphe `graph` à partir de votre fichier JSON avant d'appeler la fonction centrality_of_an_actor(graph).


#5
def determiner_distance_max_entre_toute_paire_dacteurs_dactrices(graph):
    # Vérifier si le graphe est connecté
    if nx.is_connected(graph):
        # Calculer la plus grande distance entre toute paire d'acteurs
        diameter = nx.diameter(graph)
        print("Plus grande distance entre toute paire d'acteurs :", diameter)
    else:
        # Calculer la plus grande distance entre toute paire d'acteurs pour chaque composant connecté
        components = nx.connected_components(graph)
        for component in components:
            subgraph = graph.subgraph(component)
            diameter = nx.diameter(subgraph)
            print("Plus grande distance entre toute paire d'acteurs/d'actrices dans le composant :", diameter)


def centre_du_groupe(G):
    centralities = nx.closeness_centrality(G)
    center = max(centralities, key=centralities.get)
    return center

def sous_graphe_collaborateurs_proches(G, v, k):
    if v not in G.nodes:
        print(v, "est un illustre inconnu")
        return None

    collaborateurs = set()
    collaborateurs.add(v)

    for i in range(k):
        collaborateurs_directs = set()
        for c in collaborateurs:
            for voisin in G.adj[c]:
                if voisin not in collaborateurs:
                    collaborateurs_directs.add(voisin)
        collaborateurs = collaborateurs.union(collaborateurs_directs)

    subgraph = G.subgraph(collaborateurs)
    
    # Dessiner le sous-graphe
    nx.draw(subgraph, with_labels=True, node_color='lightblue', edge_color='gray', font_weight='bold')

    # Afficher le sous-graphe dans une fenêtre
    plt.show()
    
    return subgraph





def main():
    with open('data copy 2.json') as json_data:
        graph, actors_dico = fichier_json_vers_networkx(json_data.read())
        afficher_networkx(graph)
        print(actors_dico)
        print(collab_en_communs(graph, '[[Arnold Schwarzenegger]]','[[George Clooney]]'))
        print(collaborateurs_proches(graph, '[[Arnold Schwarzenegger]]', 1))
        print(trouve_distance_entre_2_acteurs(graph, '[[Tadao Takashima]]','[[Yoshio Tsuchiya]]'))
        centrality_of_an_actor(graph)
        determiner_distance_max_entre_toute_paire_dacteurs_dactrices(graph)
        centre_du_groupe(graph)
        sous_graphe_collaborateurs_proches(graph, '[[Arnold Schwarzenegger]]', 1)

if __name__ == '__main__':
    main()

