import json
import networkx as nx
import matplotlib.pyplot as plt

# Extrait JSON
json_data = open('data copy 2.json')

# Charger les données JSON
content=json_data.readlines()
# Créer un graphe
graph = nx.Graph()


for movie in content:
    title = movie['title']
    cast = movie['cast']
    directors = movie['directors']
    producers = movie['producers']
    companies = movie['companies']

# Ajouter le titre comme nœud
graph.add_node(title)

# Ajouter les acteurs, réalisateurs, producteurs et entreprises en tant que nœuds
graph.add_nodes(title, type='movie')
for actor in cast:
    graph.add_nodes(actor, type='actor')
    graph.add_edges(title, actor, relationship='cast')
for director in directors:
    graph.add_nodes(director, type='director')
    graph.add_edges(title, director, relationship='directors')
for producer in producers:
    graph.add_nodes(producer, type='producer')
    graph.add_edges(title, producer, relationship='producers')
for company in companies:
    graph.add_nodes(company, type='company')
    graph.add_edges(title, company, relationship='companies')


# Dessiner le graphe
nx.draw(graph, with_labels=True)
plt.show()