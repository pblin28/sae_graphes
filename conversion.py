import json
import re
import networkx as nx
import matplotlib.pyplot as plt


# Créer un graphe
graph = nx.Graph()


# Extrait JSON
with open('data copy 2.json') as json_data:
    # Charger les données JSON
    content = json_data.read()
    data = json.loads(content)


        # Récupérer les acteurs
    actors = data['cast']
    for actor in actors:
        # Extraire le nom de l'acteur sans les balises
        actor_name = re.search(r'\[\[(.*?)\]\]', actor).group(1)
        graph.add_node(actor_name)

        # Créer des arêtes entre les acteurs
        for actor1 in actors:
            actor1_name = re.search(r'\[\[(.*?)\]\]', actor1).group(1)
            for actor2 in actors:
                actor2_name = re.search(r'\[\[(.*?)\]\]', actor2).group(1)
                if actor1 != actor2:
                    graph.add_edge(actor1_name, actor2_name)

# Dessiner le graphe
nx.draw(graph, with_labels=True)
plt.show()
