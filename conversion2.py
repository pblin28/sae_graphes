import json
import networkx as nx
import matplotlib.pyplot as plt

# Extrait JSON
with open('data copy 2.json') as json_data:
    # Charger les données JSON
    content = json_data.read()
    data = json.loads(content)

# Créer un graphe
graph = nx.Graph()

# Parcourir les objets JSON
for item in data:
    title = item["title"]
    cast = item["cast"]
    directors = item["directors"]
    producers = item.get("producers", [])  # Utiliser get() pour obtenir une liste vide si la clé n'existe pas
    companies = item.get("companies", [])  # Utiliser get() pour obtenir une liste vide si la clé n'existe pas

    # Ajouter le titre comme nœud
    graph.add_node(title)

    # Ajouter les acteurs, réalisateurs, producteurs et entreprises en tant que nœuds
    graph.add_nodes_from(cast)
    graph.add_nodes_from(directors)
    graph.add_nodes_from(producers)
    graph.add_nodes_from(companies)

    # Ajouter des arêtes entre le titre et les acteurs, réalisateurs, producteurs et entreprises
    graph.add_edges_from([(title, actor) for actor in cast])
    graph.add_edges_from([(title, director) for director in directors])
    graph.add_edges_from([(title, producer) for producer in producers])
    graph.add_edges_from([(title, company) for company in companies])

# Dessiner le graphe
nx.draw(graph, with_labels=True)
plt.show()
